import React, { Component } from "react";

import Menubar from "./pages/componets/Menubar";
import Home from "./pages/WebApp/Home";
import AboutUs from "./pages/WebApp/AboutUs";
import Login from "./pages/WebApp/Login";
import Manage from "./pages/WebApp/Manage";
import Credit from "./pages/WebApp/Credit";
import ManageUser from "./pages/WebApp/MitarbeiterVerwalten/ManageUser";
import ManageProduct from "./pages/WebApp/ProdukteVerwalten/ManageProduct";
import LoginRasp from "./pages/Raspi/loginRasp";
import Rasphome from "./pages/Raspi/Rasphome";
import TMain from "./pages/Raspi/TerminalMain";
import RFIDScan from "./pages/Raspi/RFIDscan";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";


function App() {
  return (
    <>
     <Router>
        <Routes>
          <Route path="/loginRasp" element={<LoginRasp />} />
          <Route path="/Raspberryhome" element={<Rasphome/>}> </Route>
          <Route path="/terminal" element={<TMain />} />
          <Route path="/rfidscan" element={<RFIDScan />} />
          <Route path="/*" element={<Menubar />} />
        </Routes>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/verwaltung" element={<Manage />} />
          <Route path="/kontostand" element={<Credit />} />
          <Route path="/ueberuns" element={<AboutUs />} />
          <Route path="/verwaltung/produkte" element={<ManageProduct />} />
          <Route path="/verwaltung/mitarbeiter" element={<ManageUser />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
