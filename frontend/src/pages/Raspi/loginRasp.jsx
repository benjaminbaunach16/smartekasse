import React, { useState, useRef, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../../services/auth.service";
import "./loginRasp.css";
import axios from "axios";

const required = (value) => {
  if (!value) {
    return <div className="text-red-600 text-xs">This field is required!</div>;
  }
};

const LoginRasp = () => {
  let navigate = useNavigate();

  const form = useRef();
  const checkBtn = useRef();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const [rfidData, setRfidData] = useState("");
  const [modalOpen, setModalOpen] = useState(false);

  const [currentUser, setCurrentUser] = useState("");

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setCurrentUser(user);
    }
  }, []);

  console.log(currentUser);

  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleLogin = (e) => {
    e.preventDefault();

    setMessage("");

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      AuthService.login(username, password).then(
        () => {
          navigate("/Raspberryhome");
          window.location.reload();
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setMessage(resMessage);
        }
      );
    }
  };

  const handleRfidLogin = (e) => {
    e.preventDefault();
    // Send an HTTP request to the backend to retrieve RFID data
    setModalOpen(true);
    fetch("http://localhost:5000/retrieve-rfid-data")
      .then((response) => response.json())
      .then((rfidData) => {
        // Check if the RFID ID was found in the database
        if ("error" in rfidData) {
          setMessage(rfidData.error);
        } else {

          AuthService.loginRFID(rfidData.user_data[0]).then(() => {
            navigate("/terminal");
        
          }, (error) => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();
  
            setMessage(resMessage);
          })
        }
      });
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };



  return (
    <>
      <div className="loginRasp-container">
        <Form onSubmit={handleLogin} ref={form}>
          <h1 className="h1-loginRasp">Melde dich an</h1>

          <div className="box1-Rasp">
            <form>
              <span className="text-center-Rasp"></span>
              {/* <input className='rounded-lg bg-gray-500  mt-2 p-1 focus:border-blure-500 focus:bg-gray-800 focus:outline-none text-white'  */}
              <div className="input-container-rasp">
                <Input
                  className="inputs"
                  type="text"
                  name="username"
                  placeholder="  username"
                  value={username}
                  onChange={onChangeUsername}
                  validations={[required]}
                />
              </div>
            </form>
          </div>

          <div>
            <form>
              <div className="input-container-rasp">
                {/* <input className='rounded-lg bg-gray-500  mt-2 p-1 focus:border-blure-500 focus:bg-gray-800 focus:outline-none text-white'  */}
                <Input
                  className="inputs"
                  type="password"
                  name="password"
                  placeholder="  password"
                  value={password}
                  onChange={onChangePassword}
                  validations={[required]}
                />
              </div>
            </form>
          </div>

          <div className="loginRasp-button-div">
            <button className="loginRasp-button">
              <span>Login</span>
            </button>
          </div>

          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
        <div>
          {/* Modales Fenster-Inhalt */}
          {modalOpen && (
            <div className="modal-overlay">
              <div className="modal-content">
                <p>Bitte legen Sie den RFID-Chip auf.</p>
                {message && (<p>{message}</p> )}
                <div className="modal-close" onClick={handleCloseModal}>
                  <i className="fas fa-times" />
                </div>
              </div>
            </div>
          )}
          <button onClick={handleRfidLogin} className="rf-login-button">
            Rfid Login
          </button>
        </div>
      </div>
    </>
  );
};

export default LoginRasp;
