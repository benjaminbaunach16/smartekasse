import React ,{useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import ProductService from "../../services/product.service"
import UserService from '../../services/user.service';
import AuthService from '../../services/auth.service';
import { useNavigate } from 'react-router-dom';

export default function TMain() {


const datas =[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

//const datas = ProductService.getProductsList();

const [data, setData]= useState(datas);
const [amount, setAmount] = useState(0);
const [products, setProducts] = useState([]);

const currentUserLogedIn = AuthService.getCurrentUser();

const [currentUserId, setCurrentUserId] = useState( currentUserLogedIn.user_id   );

const [count, setCount] = useState({})

const [currentUserCredit, setCurrentUserCredit] = useState(0);

const initUserUpdate = {credits:""};

const [userUpdate, setUserUpdate] = useState(initUserUpdate);

useEffect(() => {
     findAllProducts();
     aufNull();
     getUserCredit()
    }, []);
  
    const aufNull = () =>{

      data.map((item, i) => {
                  count[i] = (0);
            })
    }

    const findAllProducts = () => {
      ProductService.getProductsList()
        .then((response) => {
          setProducts(response.data);
          console.log(response.data);
        })
        .catch((e) => {
          console.log(e);
        });
    };
         
    const getUserCredit = () => {
      UserService.getUserCredit(currentUserId)
        .then((response) => {
          setCurrentUserCredit(response.data);
          console.log(response.data);
        })
        .catch((e) => {
          console.log(e);
        });
    };


const InkrementCount = (index) => (e) => {
      const newArray = data.map((item, i) => {
                  
            if (index === i ) 
            {

                  {products.map((product,indexx) =>{
                    if(index === indexx)
                    {  
                        setAmount( amount + product.costs);  
                    }
                  })}
              
                  count[index] += 1; 
                  return { ...item, [e.target.countt]: e.target.countt };
            } 
            else 
            {
                  return item;
            }
      });
 
setData(newArray);
};


const DekrementCount = (index) => (e) => {
      const newArray = data.map((item, i) => {
                  
            if (index === i && count[index] > 0) 
            {

                  {products.map((product,indexx) =>{
                        if(index === indexx)
                        {
                          
                            setAmount( amount - product.costs);
                          
                        }
                      })}
              
                      
                  count[index] -= 1; 
                  return { ...item, [e.target.countt]: e.target.countt };
            } 
            else 
            {
                  return item;
            }
      });
      
setData(newArray);
};

const updateUser = () =>{
      var data = {
           credits: currentUserCredit.credits - amount,
          };

          for (let key in data) {
            if (data[key] == "") {
              data[key] = userUpdate.key;
            }
          }

          UserService.updateUserCredit(currentUserId, data)
          .then((response) => {
            console.log(response.data);
          })
          .catch((e) => {
            console.log(e);
          });    
    window.open('/rfidscan','_self')
}

return (
    
<div>
      <div className='mt-5 mb-5 flex flex-row justify-center items-center  p-2 bg-gray-400 ' >
            <Link  className=' ml-10 absolute left-0 ' to="/rfidscan"  >
                  <label className=' rounded-lg bg-gray-800 border-gray-800 border-4   mt-2 p-2  font-bold text-2xl text-white ' type="text " >Ausloggen</label>
            </Link>

           
            <label className=" rounded-lg bg-gray-800 border-gray-800 border-4   mt-0 p-2   font-bold text-2xl text-white">
             Dein Kontostand: {currentUserCredit.credits} Euro
            </label>
           
            <Link onClick={updateUser} className=' mr-10 absolute right-0'  to="/rfidscan" > 
                  <label className='rounded-lg bg-gray-800  border-gray-800 border-4 mt-2 p-2  font-bold text-2xl text-white ' type="text" >Bestätigen</label>
            </Link>
      </div>

<div className=' grid grid-cols-3 mt-3 gap-5 content-start   '>


{products.map((product,index) =>{
           
              return <div  className=' box text-white  min-w-full text-4xl text-center bg-[#3a3a3a] rounded-lg p-5' key={index} >

                          <button onClick={InkrementCount(index)}> 
                              <b >{product.name}</b>
                              <b className='ml-16 border-2 border-white rounded-lg p-1'>{count[index]}</b> 
                              <br />{product.costs}<b> €</b><br /> 
                        </button>

                              <div className='flex items-center justify-center'>
                                    <button className='rounded-lg border-2 border-white mr-0 w-2/6' onClick={DekrementCount(index)} >-</button>
                                    <button className='rounded-lg border-2 ml-10 w-2/6' onClick={InkrementCount(index)} >+</button>   
                              </div>
                    
            </div>
              })}
  
      </div>



</div>
)}
