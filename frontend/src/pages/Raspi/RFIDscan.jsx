import React  from "react";
import { NavLink } from "react-router-dom";
import rfid from "./rfid.png";

export default function RFIDScan(){
    
    return(
      
        <div  className="fixed inset-0 flex justify-center items-center ">

         <NavLink to="/terminal">
            <div className=""> 
                <img src={rfid} width="60%" alt="person"/> 
            </div>
            
             <div className=" rounded-lg p-2 text-5xl text-white bg-black">
                Scan deinen RFID-Chip
            </div>
        </NavLink>

        <NavLink to="/PinEntry">    
             <div className=" rounded-lg p-2 text-5xl text-white bg-black">
               Pin eingeben
            </div>
        </NavLink>
        </div>
    )
}
