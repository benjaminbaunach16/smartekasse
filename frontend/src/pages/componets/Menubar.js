import { Link, useLocation } from "react-router-dom";
import React, { useState, useEffect } from "react";
import "./Menubar.css";


import AuthService from "../../services/auth.service";

const Menubar = () => {
  const [currentUser, setCurrentUser] = useState(undefined);
  const [adminUser, setAdminUser] = useState(false);

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
      setAdminUser(user.is_admin);
    }
  }, []);

  const logOut = () => {
    AuthService.logout();
    setAdminUser(false);
    setCurrentUser(undefined);
    closeMobileMenu();
  };

  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };


  // nach pfaden prüfen
  function useShouldRenderMenubar() {
    const location = useLocation();

    if(location.pathname == '/loginRasp' || location.pathname == "/Raspberryhome" || location.pathname == '/terminal' || location.pathname == '/rfidscan') return null;
    else return true;
  }


 
  const shouldRender = useShouldRenderMenubar();
  console.log(shouldRender)



  if (!shouldRender) {
    return null;
  }
  

  window.addEventListener("resize", showButton);





  return (
    <>
      
      <nav className="navbar">
        <div className="navbar-container">
          <div className="navbar-logo"></div>
          <div className="menu-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"} />
          </div>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                Home
              </Link>
            </li>

            {currentUser && (
              <li className="nav-item">
                <Link
                  to="/kontostand"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Kontostand
                </Link>
              </li>
            )}

            {adminUser && (
              <li className="nav-item">
                <Link
                  to="/verwaltung"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Verwalten
                </Link>
              </li>
            )}

            <li className="nav-item">
              <Link
                to="/ueberuns"
                className="nav-links"
                onClick={closeMobileMenu}
              >
                Über uns
              </Link>
            </li>

            {currentUser ? (
              <li className="nav-item">
                <Link to="/" className="nav-links" onClick={logOut}>
                  {/* <a onClick={logOut}>Logout</a> */}
                  Logout
                </Link>
              </li>
            ) : (
              <li className="nav-item">
                <Link
                  to="/login"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Login
                </Link>
              </li>
            )}
          </ul>
          {/* {button && <Button buttonStyle={'btn--outline'}>SIGN UP</Button>} */}
        </div>
      </nav>
    </>
  );
};

export default Menubar;
