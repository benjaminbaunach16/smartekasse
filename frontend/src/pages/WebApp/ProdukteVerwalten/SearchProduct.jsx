import React, { useState, useEffect } from "react";
import ProductService from "../../../services/product.service";

const Psuchen = () => {
  const [products, setProducts] = useState([]);
  const [currentProduct, setCurrentProduct] = useState(null);
  const [searchName, setSearchName] = useState("");
  const [editSubmit, setEditSubmit] = useState(true);
  const [productUpdateSubmit, setProductUpdateSubmit] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    findAllProducts();
  }, []);

  const initProductUpdate = {
    user_id: null,
    username: "",
    firstname: "",
    lastname: "",
    email: "",
    is_admin: false,
  };

  const [productUpdate, setProductUpdate] = useState(initProductUpdate);

  const handleUpdateProductInput = (event) => {
    const { name, value } = event.target;
    setProductUpdate({ ...productUpdate, [name]: value });
  };

  const handleUpdateProductSubmit = () => {
    setProductUpdateSubmit(true);
  };

  const handleBackToProductSubmit = () => {
    setProductUpdateSubmit(false);
    findAllProducts();
  };

  const onChangeSearchName = (event) => {
    const searchName = event.target.value;
    setSearchName(searchName);
  };

  const refreshActiveProduct = () => {
    setCurrentProduct(null);
    setEditSubmit(true);
  };

  const setActiveProduct = (product) => {
    setCurrentProduct(product);
    setProducts([product]);
    setMessage("");
    setSearchName("");
    setEditSubmit(false);
  };

  const findAllProducts = () => {
    setMessage("Select a Product");
    ProductService.getProductsList()
      .then((response) => {
        setProducts(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
    refreshActiveProduct();
  };

  const findByName = () => {
    if (searchName) setMessage("Select a Product");
    ProductService.findByName(searchName)
      .then((response) => {
        setProducts(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
    refreshActiveProduct();
    setEditSubmit(true);
  };

  const deleteProduct = () => {
    ProductService.deleteProduct(currentProduct.product_id)
      .then((response) => {
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
    findAllProducts();
    refreshActiveProduct();
    window.location.reload(false);
  };

  const updateProduct = () => {
    var data = {
      name: productUpdate.name,
      costs: productUpdate.costs,
    };

    if (data.name === "" && data.costs === "") {
      setMessage(`Keine Veränderungen an ${currentProduct.name} vorgenommen!`);
      return;
    }

    for (let key in data) {
      if (data[key] === "") {
        data[key] = productUpdate.key;
      }
    }

    ProductService.updateProduct(currentProduct.product_id, data)
      .then((response) => {
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
    findAllProducts();
    refreshActiveProduct();
    setProductUpdate(initProductUpdate);
    setProductUpdateSubmit(false);
    window.location.reload(false);
  };

  return (
    <div className="bg-gray-800  w-1/3 float-right mr-20 mt-10">
      <div className="bg-gray-800 text-zinc-50 text-2xl text-center ">
        PRODUKTE SUCHEN
      </div>

      <div className="flex flex-col text-black py-2">
        <input
          className="rounded-lg bg-gray-600 mt-2 p-1 focus:border-blure-500 focus:bg-gray-800 focus:outline-none text-white"
          type="text"
          id="name"
          name="name"
          value={searchName}
          onChange={onChangeSearchName}
          required
        />
        <button
          className="w-1/2 my-5 py-2 bg-white  text-black rounded-lg "
          onClick={findByName}
        >
          Suchen
        </button>
        {!editSubmit && (
          <button
            className="w-1/2 my-5 py-2 bg-white  text-black rounded-lg"
            onClick={handleBackToProductSubmit}
          >
            Zurück
          </button>
        )}
        {message && <div className="text-white">{message}</div>}
        <table className="text-white">
          <thead>
            <th>Produktname</th>
            <th>Kosten</th>
            {editSubmit && <th>Bearbeiten</th>}
          </thead>
          {products &&
            products.map((product) => (
              <tbody>
                <tr>
                  <td>{product.name}</td>
                  <td>{product.costs}</td>
                  {editSubmit && (
                    <td>
                      <span onClick={() => setActiveProduct(product)}>
                        <i className="far fa-edit action mr-2"></i>
                      </span>
                    </td>
                  )}
                </tr>
              </tbody>
            ))}
        </table>
        <div>
          {currentProduct && productUpdateSubmit && (
            <div>
              <div>
                <label>
                  <strong className="text-white">Produktname:</strong>
                </label>{" "}
                <input
                  placeholder="Produktname"
                  name="name"
                  id="name"
                  required
                  value={productUpdate.name}
                  onChange={handleUpdateProductInput}
                />
              </div>
              <div>
                <label>
                  <strong className="text-white">Vorname:</strong>
                </label>{" "}
                <input
                  placeholder="Kosten"
                  name="costs"
                  id="costs"
                  required
                  value={productUpdate.costs}
                  onChange={handleUpdateProductInput}
                />
              </div>
              <button
                className="w-1/6 my-5 py-1 bg-white  text-black rounded-lg"
                onClick={updateProduct}
              >
                OK
              </button>
            </div>
          )}
          {!editSubmit && !productUpdateSubmit && (
            <div>
              <button
                className="w-1/6 my-5 py-1 bg-white  text-black rounded-lg"
                onClick={handleUpdateProductSubmit}
              >
                Produkt bearbeiten
              </button>
              <button
                className="w-1/6 my-5 py-1 bg-white  text-black rounded-lg"
                onClick={deleteProduct}
              >
                Produkt löschen
              </button>
            </div>
          )}
        </div>
      </div>
      <div className="flex justify-center"></div>
    </div>
  );
};

export default Psuchen;
