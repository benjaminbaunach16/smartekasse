import React from "react";
import { NavLink } from "react-router-dom";
import backButton from "../backButton.png";
import CreateProduct from "./CreateProduct"
import SearchProduct from "./SearchProduct"

function Pverwalten() {
  return (
    <div>
      <div className="bg-gray-800 rounded-full  ml-5 mt-5 absolute  left-0 w-1/12">
        <NavLink to="/verwaltung" style={({ isActive }) => ({})}>
          <img src={backButton} width="50%" alt="backbutton" />
        </NavLink>
      </div>

      <div className="text-zinc-50 text-5xl text-center mt-5">
        <h1>VERWALTEN - PRODUKTE </h1>
      </div>
      <CreateProduct></CreateProduct>
      <SearchProduct></SearchProduct>
    </div>
  );
}

export default Pverwalten;
