import React, { useState } from "react";
import ProductService from "../../../services/product.service";

const Phinzu = () => {
  const initProductCreate = {
    user_id: null,
    name: "",
    costs: "",
  };

  const [productCreate, setProductCreate] = useState(initProductCreate);
  const [message, setMessage] = useState("");
  const [submitted, setSubmitted] = useState(false);

  const handleCreateProductInput = (event) => {
    const { name, value } = event.target;
    setProductCreate({ ...productCreate, [name]: value });
  };

  const createProduct = () => {
    var data = {
      name: productCreate.name,
      costs: productCreate.costs,
    };

    ProductService.create(data)
      .then((response) => {
        setSubmitted(true);
        setMessage("Das Produkt wurde erfolgreich angelegt!");
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
    // newUser();
  };

  const saveNewProduct = () => {
    setProductCreate(initProductCreate);
    setMessage("");
    setSubmitted(false);
    window.location.reload(false);
  };
  return (
    <div className="bg-gray-800  w-1/3 float-left ml-20 mt-10">
      <div className="bg-gray-800 text-zinc-50 text-2xl text-center ">
        PRODUKTE HINZUFÜGEN
      </div>
      {message && <div className="text-white">{message}</div>}
      {submitted ? (
        <div className="flex justify-center">
          <button
            className="w-1/2 my-5 py-2 bg-white  text-black rounded-lg"
            onClick={saveNewProduct}
          >
            OK
          </button>
        </div>
      ) : (
        <div className="flex flex-col text-black py-2">
          <input
            className="rounded-lg bg-gray-500 mt-2 p-1 focus:border-blure-500 focus:bg-gray-800 focus:outline-none text-white"
            type="text"
            placeholder="Produktname"
            id="name"
            name="name"
            value={productCreate.name}
            onChange={handleCreateProductInput}
            required
          />
          <input
            className="rounded-lg bg-gray-500  mt-2 p-1 focus:border-blure-500 focus:bg-gray-800 focus:outline-none text-white"
            type="text"
            placeholder="Preis"
            id="costs"
            name="costs"
            value={productCreate.costs}
            onChange={handleCreateProductInput}
            required
          />

          <div className="flex justify-center">
            <button onClick={createProduct} className="w-1/2 my-5 py-2 bg-white  text-black rounded-lg ">
              HINZUFÜGEN
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Phinzu;
