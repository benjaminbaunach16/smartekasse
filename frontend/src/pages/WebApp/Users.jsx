// http://localhost:3000/
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import axios from "axios"; //API request using react app

//when user component runs, run useEffect
export const Users = () => {

    const [users,setUsers] = useState([]) //declares a “state variable”, contain data or information about the component
                                        //component's state can change over time; whenever it changes, the component re-renders
    useEffect(()=>{ //allows side effects in components,like fetching data, directly updating the DOM, and timers. two arguments

        const fetchAllUsers = async () =>{ // async bc API call, fetch all data
            try{
                const res = await axios.get("http://localhost:8800/users") //await bc async
                setUsers(res.data);
                console.log("test")
            }catch(err){
                console.log(err)
            }
        }
        fetchAllUsers();
    },[])

  return <div>
        <h1>Users in Database</h1>
        <div className="users">
            { //iterate through users in database
                users.map(user=>(
                    <div className="user">
                        <hr />
                        <h2>UserID: {user.userID}</h2>
                        <h2>Vorname: {user.firstname} Nachname: {user.lastname}</h2>
                        <h2>Anzahl an Kaffees: {user.coffeecount}</h2>
                        <h2>Email: {user.email}</h2>
                    </div>
                ))
            }
        </div>
    </div>
  
}