import React, { useState, useEffect } from "react";
import AuthService from "../../services/auth.service";
import UserService from "../../services/user.service";

function Kontostand() {
  const [currentUser, setCurrentUser] = useState(null);

  const currentUserLogedIn = AuthService.getCurrentUser();
  const [currentUserId, setCurrentUserId] = useState(
    currentUserLogedIn.user_id
  );

  useEffect(() => {
    // setCurrentUserId(currentUserLogedIn.user_id);
    getUserCredit();
  }, []);

  const getUserCredit = () => {
    UserService.getUserCredit(currentUserId)
      .then((response) => {
        setCurrentUser(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div className="bg-gray-800">
      <div className="text-zinc-50 text-6xl text-center mt-5">
        <h1>MEIN KONOSTAND</h1>
      </div>

      <div className="bg-gray-800  w-1/3 float-left ml-20 mt-10">
        <div className="bg-gray-800 text-zinc-50 text-2xl text-center ">
          ÜBERSICHT
        </div>
        <div className="flex flex-col text-white py-2">
          <div className="bg-white text-black">
            <label>KAFFEE:</label> <label> 5</label> <br />
          </div>
        </div>
      </div>

      <div className="bg-gray-600  w-1/3 float-right mr-20 mt-10">
        <div className="bg-gray-800 text-zinc-50 text-2xl text-center ">
          KONTOSTAND
        </div>
        {currentUser && (
          <div>
            <label className="text-white">
              Aktueller Kredit: {currentUser.credits} Euro
            </label>
            <br />
          </div>
        )}
      </div>
    </div>
  );
}

export default Kontostand;
