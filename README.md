# SmarteKasse


        Willkommen zu einem meiner spannendsten Projekte - einem digitalen Kaffeemanagementsystem, entwickelt für die "pep.digital GmbH" und Steinbeis. Dieses System revolutioniert die Art und Weise, 
        'wie Unternehmen ihren Kaffeeverbrauch verwalten, indem es innovative Technologien mit Benutzerfreundlichkeit verbindet. Unser Team nutzte React, eine leistungsstarke Bibliothek zur Erstellung 
        webbasierter Benutzeroberflächen, und entwickelte eine robuste API mit Node und Express. Zur Verwaltung unserer Daten haben wir MySQL, ein bewährtes Open-Source-SQL-Datenbankmanagementsystem, verwendet. 
        Für maximale Zuverlässigkeit und einfache Bereitstellung haben wir unsere Anwendung in Docker-Containern isoliert. Einzigartig an diesem Projekt ist die Verwendung der RFID-Technologie und eines Raspberry Pi-Terminals. 
        Durch die Integration eines Touchscreens und eines RFID-Scanners haben wir eine intuitive und benutzerfreundliche Schnittstelle geschaffen, die den Kaffeebezug für die Mitarbeiter einfacher und spaßiger macht als je zuvor.
        Das Projektmanagement war ein zentraler Aspekt bei der Realisierung dieses Projekts. Mit Hilfe von GitLab und agilen Methoden konnten wir effizient zusammenarbeiten, Herausforderungen meistern und das Projekt erfolgreich zum Abschluss bringen.
        Dieses digitale Kaffeemanagementsystem repräsentiert meine Leidenschaft für kreative Problemlösungen und die Anwendung modernster Technologien. Entdecken Sie die Details und lassen Sie sich von der Technik hinter dem Spaß am Kaffee begeistern.