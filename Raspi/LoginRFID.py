import mysql.connector
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import requests

# Connect to database
cnx = mysql.connector.connect(
    user="root",
    password="test123",
    host="localhost",
    database="pswtm_db"
)

# Create cursor for database operations
cursor = cnx.cursor()

# Initialize RFID reader
reader = SimpleMFRC522()

try:
    # Read RFID chip and store ID in variable
    id, text = reader.read()

    # Check if RFID ID exists in database
    query = "SELECT * FROM tags WHERE id = %s"
    cursor.execute(query, (id,))
    result = cursor.fetchone()

    if result:
        # RFID ID exists in database, so login is successful
        print("Login successful")
    else:
        # RFID ID does not exist in database, so login is not successful
        print("Login not successful")

finally:
    # Clean up RFID reader
    GPIO.cleanup()

# Close database connection
cnx.close()


# Send HTTP request to React frontend
response = requests.post("http://localhost:3000/transferRfid", data={"rfid": id})

# Check if login was successful based on response from frontend
if response.status_code == 200:
    # Login was successful
    print("Login successful")
else:
    # Login was not successful
    print("Login not successful")