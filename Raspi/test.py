import json
import requests

test = "123456"

def transferTestToReact(test):
  # Send test data to React component via HTTP request
  url = "http://localhost:5000/transferTest"
  data = {"test": test}
  headers = {"Content-Type": "application/json"}
  requests.post(url, data=json.dumps(data), headers=headers)

transferTestToReact(test)
