import mysql.connector
import RPi.GPIO as GPIO

from mfrc522 import SimpleMFRC522


try:
	cnx =mysql.connector.connect(
		user = "root",
		password = "test123",
		host = "localhost",
		database = "pswtm_db"
		)
	print('Verbindung erfolgreich')

except mysql.connector.Error as err:
	if err.errno == mysql.connector.errorcode.ER_ACCES_DENIED_ERROR:
		print("user oder pw falsch")
	elif err.errno == mysql.connector.errorcode.ER_BAD_DB_ERROR:
		print("Db gibts nicht")
	else: 
		print(err)		
	

cursor = cnx.cursor() 



#in Datenbank scchreiben
#query = "INSERT INTO table1 (name, value)" "VALUES (%s, %s)"
#cursor.execute(query, ('benny' , 100))
#cnx.commit()


# von Datenbank lesen
#query = 'SELECT * FROM table1'
#cursor.execute(query)


#for (id, name, value) in cursor: 
	#print (f'id: {id}, name: {name}, value: {value}')
	
	
#RFID erstellen 
reader = SimpleMFRC522()

	
try:
	id, text = reader.read()

	#testen ob id schon da
	query = 'SELECT id FROM tags WHERE id = %s'
	cursor.execute(query, (id,))
	result = cursor.fetchone()
	
	if not result:	
		query = 'INSERT INTO tags (id) VALUES (%s)'
		cursor.execute(query, (id,))
		cnx.commit()
	
finally: 
	GPIO.cleanup


			
#Alle IDs auslesen 
query = 'SELECT id FROM tags'
cursor.execute(query)

#ID ausgeben
for (id,) in cursor:
	print(f'ID: {id}')


	
	
cnx.close()
