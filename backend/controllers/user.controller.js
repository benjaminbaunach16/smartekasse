/*
  User Controller
  Requests for User Functions
*/
const db = require("../entity/index.entity");
const UserEntity = db.users;
const Op = db.Sequelize.Op;

var bcrypt = require("bcryptjs");

//POST, create user and save in db
exports.createUser = (req, res) => {
  //create a user
  const user = {
    username: req.body.firstname + "." + req.body.lastname,
    password: bcrypt.hashSync("firstLogin", 8),
    firstname: req.body.firstname,
    credits: 0,
    lastname: req.body.lastname,
    email: req.body.email,
    is_admin: req.body.is_admin,
  };

  //save the created user in database
  UserEntity.create(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating user.",
      });
    });
};

//GET, find users by username
exports.findByUsername = (req, res) => {
  const username = req.query.username;
  var condition = username
    ? { username: { [Op.like]: `%${username}%` }, is_admin: { [Op.eq]: false } }
    : null;

  //search for user only if conidtion is given
  if (condition != null) {
    UserEntity.findAll({ where: condition })
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving user.",
        });
      });
  } else {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving user.",
    });
  }
};

//PUT, update a user with user_id
exports.updateUser = (req, res) => {
  const user_id = req.params.user_id;

  UserEntity.update(req.body, {
    where: { user_id: user_id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update User with user_id=${user_id}. Maybe User was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating User with user_id=" + user_id,
      });
    });
};



//PUT, update a user_credit with user_id
exports.updateUserCredit = (req, res) => {
  const user_id = req.params.user_id;

  UserEntity.update(  req.body, {
    where: { user_id: user_id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update User with user_id=${user_id}. Maybe User was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating User with user_id=" + user_id,
      });
    });
};



//DELETE, delete a user with user_id
exports.deleteUser = (req, res) => {
  const user_id = req.params.user_id;

  UserEntity.destroy({
    where: { user_id: user_id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete User with user_id=${user_id}. Maybe User was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete User with user_id=" + user_id,
      });
    });
};

exports.getUsersList = (req, res) => {
  UserEntity.findAll({ where: { is_admin: false } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};

exports.getUserCredit = (req, res) => {
  const user_id = req.params.user_id;

  UserEntity.findOne({ where: { user_id: user_id } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};
