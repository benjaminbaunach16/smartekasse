const express = require('express');
const cors = require("cors"); // data transfers between browsers and servers

const app = express();
app.use(express.json())
app.use(cors())

var corsOptions = { //for example can define host of frontend
    origin: "http://localhost:3000"
  };

// provides Express middleware to enable CORS with various options
app.use(cors(corsOptions));

// parse request data content type application/json
app.use(express.json());

// parse request data content type application/x-www-form-rulencoded
app.use(express.urlencoded({extended: true}));

//import sequelized db with objects
const db = require("./entity/index.entity.js");

// drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

// creates tables if doesn't exist (does nothing if already exists)
db.sequelize.sync()
    .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
});

// simple home route
app.get("/",(req,res)=>{
    res.json({ message: "Welcome to backend application." });
})

require("./routes/user.routes")(app);
require("./routes/auth.routes")(app);
require("./routes/product.routes")(app);


const PORT = process.env.PORT || 8800;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

